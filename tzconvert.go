package main

import (
	"time"
	"regexp"
	"errors"
	"encoding/csv"
	"io"
	"net/http"
	"os"
	"strings"
	"log"
	"fmt"
)

func main() {
	now := time.Now()

	args := os.Args[1:]
	fromTime, fromCode, toCode := parseArgs(args, &now)

	fromCity, toCity := readCityData(fromCode, toCode)

	if toCity == nil {
		log.Fatal("Error: Destination required")
	}

	toTime, err := convert(fromCity, toCity, fromTime, &now)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%s (%s) %s\n", toCity.Name, toCity.Country, formatTime(toTime))
}

func parseArgs(args []string, refTime *time.Time) (*time.Time, string, string) {
	fromTime := refTime
	fromCode, toCode := "", ""
	for _, arg := range args {
		fr, to, err := parseCity(arg)
		if err == nil {
			fromCode = fr
			toCode = to
		} else {
			ti, err := parseTime(arg)
			if err == nil {
				fromTime = ti
			}
		}
	}
	return fromTime, fromCode, toCode
}

func readCityData(fromCode, toCode string) (*CityData, *CityData) {
	fromCity, toCity := checkMatch(fromCode, nil, utc), checkMatch(toCode, nil, utc)
	resp, err := http.Get("https://raw.githubusercontent.com/jpatokal/openflights/master/data/airports.dat")
	defer resp.Body.Close()
	if err != nil {
		return nil, nil
	}
	r := csv.NewReader(resp.Body)
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, nil
		}
		c := NewCityData(record)

		fromCity = checkMatch(fromCode, fromCity, c)
		toCity = checkMatch(toCode, toCity, c)
	}
	return fromCity, toCity
}

func checkMatch(code string, lastMatch, candidate *CityData) *CityData {
	if code == "" {
		return lastMatch
	}
	if code == utc.Name {
		return utc
	}
	if code == candidate.IataCode {
		return candidate
	}
	upperName := strings.ToUpper(candidate.Name)
	if lastMatch == nil && strings.HasPrefix(upperName, code) {
		return candidate
	}
	return lastMatch
}

var cityRegex = regexp.MustCompile("((\\w{3})[\\W\\D\\S])?(\\w{3})")

func parseCity(input string) (string, string, error) {
	m := cityRegex.FindStringSubmatch(input)
	if m != nil {
		return strings.ToUpper(m[2]), strings.ToUpper(m[3]), nil
	}
	return "", "", errors.New("Unable to parse input: " + input)
}

type CityData struct {
	Name     string
	Timezone string
	Country  string
	IataCode string
}

func NewCityData(record []string) *CityData {
	c := new(CityData)
	c.Name = record[2]
	c.Timezone = record[11]
	c.Country = record[3]
	c.IataCode = record[4]
	return c
}

var utc = &CityData{"UTC", "UTC", "Coordinated Universal Time", ""}

func timeAtLocation(input, ref *time.Time, loc *time.Location) *time.Time {
	t := time.Date(ref.Year(), ref.Month(), ref.Day(), input.Hour(), input.Minute(), 0, 0, loc)
	return &t
}

func parseTime(input string) (*time.Time, error) {
	t, err := time.Parse("15:04", input)
	return &t, err
}

func formatTime(t *time.Time) string {
	return t.Format("15:04")
}

func convert(fromCity, toCity *CityData, fromTime, refTime *time.Time) (*time.Time, error) {
	fromLoc, err := getLocation(fromCity)
	if err != nil {
		return nil, err
	}
	fromAtLocation := timeAtLocation(fromTime, refTime, fromLoc)

	toLoc, err := getLocation(toCity)
	if err != nil {
		return nil, err
	}
	toAtLocation := fromAtLocation.In(toLoc)
	return &toAtLocation, nil
}

func getLocation(cityData *CityData) (*time.Location, error) {
	if cityData == nil {
		return time.Local, nil
	}
	return time.LoadLocation(cityData.Timezone)
}
