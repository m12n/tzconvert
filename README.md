# tzconvert

A tool to convert times to different timezones.

## Installation

    go get -u github.com/m12n/tzconvert/go/tzconvert
    
## Usage

    tzconvert [from:]to [time]

- `from` the IATA code or first 3 letters of the name of the origin city
- `to` the IATA code or first 3 letters of the name of the destination city
- `time` the time to be converted from origin to destination, should be in the format `HH:MM`

`from` or `to` can also be `UTC`.

When omitting `from`, then the timezone of the system is used.
When omitting the `time`, then the current system time is used.

## Examples

    tzconvert 15:00
    
Gives an error; a destination city is requried.

    tzconvert nyc

Will convert the current time to the timezone of New York City.

    tzconvert del:nyc 15:00
    
Will convert the local time 3pm at Delhi / India to the timezone of New York City.

## Data Source

It downloads the IATA / timezone mapping data from https://raw.githubusercontent.com/jpatokal/openflights/master/data/airports.dat 
