package main

import (
	"testing"
	"time"
)

func TestParseArgs(t *testing.T) {
	args := []string{"ham"}
	refTime := time.Time{}
	expectedTime, expectedFrom, expectedTo := &refTime, "", "HAM"
	fromTime, fromCode, toCode := parseArgs(args, &refTime)
	if (expectedTime != fromTime) {
		t.Error("not expected time", expectedTime, fromTime)
	}
	if (expectedFrom != fromCode) {
		t.Error("not expected from", expectedFrom, fromCode)
	}
	if (expectedTo != toCode) {
		t.Error("not expected to", expectedTo, toCode)
	}
}

func TestCheckMatch(t *testing.T) {
	ham, nyc := &CityData{"Hamburg", "", "", "HAM"}, &CityData{"New York City", "", "", "NYC"}

	assertCheckMatch(t, "Hamburg", "HAM", nil, ham)
	assertCheckMatch(t, "New York City", "NYC", ham, nyc)
	assertCheckMatch(t, "New York City", "NYC", nil, nyc)
	assertCheckMatch(t, "New York City", "NEW", nil, nyc)
	assertCheckMatch(t, "Hamburg", "NEW", ham, nyc)
}

func assertCheckMatch(t *testing.T, expectedName, code string, lastMatch, candidate *CityData) {
	r := checkMatch(code, lastMatch, candidate)
	if r == nil || r.Name != expectedName {
		t.Error("not expected", r, expectedName, code, lastMatch, candidate)
	}
}

func TestParseCity(t *testing.T) {
	assertParseCity(t, "ham", "", "HAM", true)
	assertParseCity(t, "HAM", "", "HAM", true)
	assertParseCity(t, "ABC:DEF", "ABC", "DEF", true)
	assertParseCity(t, "12:30", "", "", false)
}

func assertParseCity(t *testing.T, input string, fromExpected string, toExpected string, success bool) {
	from, to, err := parseCity(input)
	if err != nil {
		if success {
			t.Error("Error", input, fromExpected, toExpected, err)
		}
	} else {
		if !success {
			t.Error("Error expected", input, fromExpected, toExpected, err)
		}
		if from != fromExpected {
			t.Error("From not expected", input, fromExpected, toExpected, from)
		}
		if to != toExpected {
			t.Error("To not expected", input, fromExpected, toExpected, to)
		}
	}
}

func TestParseTime(t *testing.T) {
	assertParseTime(t, "10:30", true, 10, 30)
	assertParseTime(t, "abc", false, 0, 0)
}

func assertParseTime(t *testing.T, input string, isTime bool, hour int, minute int) {
	ti, err := parseTime(input)
	if err == nil {
		if !isTime {
			t.Fail()
		}
		if ti.Hour() != hour {
			t.Error("hour", hour, ti.Hour())
		}
		if ti.Minute() != minute {
			t.Error("minute", minute, ti.Minute())
		}
	} else {
		if isTime {
			t.Fail()
		}
	}
}
